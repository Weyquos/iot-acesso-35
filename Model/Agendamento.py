class Agendamento:

    def __init__(self, idagendamento, cliente, contrato, inicio, fim, iotid, iniciado, encerrado, andamento):
        self.id = idagendamento
        self.responsavel = cliente
        self.contrato = contrato
        self.inicio = inicio
        self.fim = fim
        self.iotid = iotid
        self.iniciado = iniciado
        self.encerrado = encerrado
        self.andamento = andamento

