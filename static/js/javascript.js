var pin = new Array();
var modo = 0;
var user = 0;

function abrirTeclado() {
    $.getJSON('/_openKeyb',  function(data) {
        //document.getElementById('con').innerHTML = data.toString();
    });
}

//Caso exista conexão com o leitor, inicia-se o processo de update do leitor biometrico
function update(){
    document.getElementById('loading').style.display = 'block';
    $.getJSON('/_update',  function(data) {
        if(data == 0){
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }else{
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }
    });
}

function updateApp(){
    document.getElementById('loading').style.display = 'block';
    $.getJSON('/att_app',  function(data) {
        if(data == 0){
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }else{
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }
    });
}

//Valida a permissão do pin informado. Caso possua permissão a porta é liberada
function entrar(pin){
    $.getJSON('/_validate_pin', {pin:pin} ,function(data) {
        if(data['result'] == 3 || data['result'] == 1){
            $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Bem Vindo!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
            $.getJSON('/Fechar', function(data) {
            });
        }else{
            //Informar que o PIN é invalido
            $('#resultado').html("<button type='button' class='btn btn-danger btn-circle btnResult'><i class='far fa-times-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Pin Incorreto!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
            $(".passinput").val("");
        }
    });
}

function entrarTecladoPin(pinEntrada){
    $.getJSON('/_validate_pin', {pin:pinEntrada} ,function(data) {
        if(data['result'] == 3 || data['result'] == 1){
            $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Bem Vindo!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
            $.getJSON('/Fechar', function(data) {
            });
        }else{
            //Informar que o PIN é invalido
            $('#resultado').html("<button type='button' class='btn btn-danger btn-circle btnResult'><i class='far fa-times-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Pin Incorreto!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
            $(".passinput").val("");
        }
    });
}

//Reinicia o dispositivo
function reboot(){
    $.getJSON('/reboot',  function(data) {
    });
}


//======================================== Cadastro Biometrico ========================================//

//Coloca o leitor biometrico no modo de cadastro
function Cadastrar(){
    modo = 1;
    closeOp();
    tecladoCadastro();
    //$('#pModo').html('Pin para Cadastro');
}

//Valida se o pin informado é valido para o cadastro biometrico
function validarPinCadastro(pin){
    $.getJSON('/_validate_pin_cadastro', {pin:pin} ,function(data) {
        $(".passinput").val("");
        if(data != 0 && data != -1 && data != null){
            user = data['cod'];
            $('#resultado').html("<div class='loader'></div>");
            $('#resultadoText').html('Iniciando Cadastro');
            document.getElementById('ModalAviso').style.display = 'block';
            $.getJSON('/_count_digitais', {user:user}, function(data) {
                if(data != -1){
                    digital = data;
                    if(data == 2){
                        falhaCadastroBio('Quantidade Maxima de Digitais Cadastradas');
                        return;
                    }else{
                        setTimeout(function() {$('#resultadoText').html('Coloque o dedo sobre o leitor');}, 1000);
                        $.getJSON('/_read_finger0', function(dataf0) {
                            if(dataf0 == 1){
                                $('#resultadoText').html('Remova o dedo do leitor.');
                                sleep(3000).then(() => {
                                    $('#resultadoText').html('Coloque novamente o mesmo dedo sobre o leitor');
                                    $.getJSON('/_read_finger1',{user:user,digital:digital},function(data) {
                                        if(data == -1){
                                            falhaCadastroBio('Digital já cadastrada');
                                            user ='';
                                        }else if(data == 0){
                                            falhaCadastroBio('Erro ao realizar cadastro');
                                            user ='';
                                        }else if(data == 1){
                                            $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
                                            $('#resultadoText').html('Cadastro realizado com sucesso!');
                                            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
                                            setTimeout(function() {$('#resultadoText').html('');}, 4000);
                                            user ='';
                                        }else if(data == 2){
                                            falhaCadastroBio('Amostras Diferentes');
                                            user ='';
                                        }else{
                                            falhaCadastroBio('Erro ao realizar cadastro');
                                            user ='';
                                        }
                                    });
                                });
                            }else{
                                user ='';
                                falhaCadastroBio('Leitor biometrico não esta conectado');
                            }
                        });
                    }
                }else{
                    user ='';
                    falhaCadastroBio('Leitor biometrico não esta conectado');
                }
              });
        }else if(data == 0){
            falhaCadastroBio('Sem conexão, tente novamente mais tarde.');
        }else if(data == -1){
            falhaCadastroBio('Nenhum usuario encontrado para o pin informado.');
        }else{
            falhaCadastroBio('Nenhum usuario encontrado para o pin informado.');
        }
    });
}

//Coloca o leitor biometrico no modo de cadastro
function CadastrarFuncionario(){
    $.getJSON('/cadastro',  function(data) {
        modo = 4;
        closeOp();

    });
}

//Valida se o pin informado é valido para o cadastro biometrico
function validarPinCadastroFuncionario(codigo){
    document.getElementById('ModalTecladoOp').style.display = 'none';
    $.getJSON('/_validate_pin_cadastro_funcionario', {codigo:codigo} ,function(data) {
        pin = [];
        $(".passinput").val("");
        if(data != 0 && data != -1 && data != null){
            user = data['cod'];
            $('#resultado').html("<div class='loader'></div>");
            $('#resultadoText').html('Iniciando Cadastro');
            document.getElementById('ModalAviso').style.display = 'block';
            $.getJSON('/_count_digitais_funcionario', {user:user}, function(data) {
                if(data != -1){
                    digital = data;
                    if(data == 2){
                        falhaCadastroBio('Quantidade Maxima de Digitais Cadastradas');
                        return;
                    }else{
                        setTimeout(function() {$('#resultadoText').html('Coloque o dedo sobre o leitor');}, 3000);
                        $.getJSON('/_read_finger0', function(dataf0) {
                            if(dataf0 == 1){
                                $('#resultadoText').html('Remova o dedo do leitor.');
                                setTimeout(function() {$('#resultadoText').html('Coloque novamente o mesmo dedo sobre o leitor');}, 3000);
                                setTimeout(function() {
                                    $.getJSON('/_read_finger1_funcionario',{user:user,digital:digital},function(data) {
                                        if(data == 1){
                                            $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
                                            $('#resultadoText').html('Cadastro realizado com sucesso!');
                                            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
                                            setTimeout(function() {$('#resultadoText').html('');}, 4000);
                                            user ='';
                                            modo = 0;
                                            $('#pModo').html('Pin de Acesso');
                                        }else{
                                            falhaCadastroBio('Erro ao realizar cadastro');
                                            user ='';
                                        }
                                    });}, 4000);
                            }else{
                                user ='';
                                falhaCadastroBio('Leitor biometrico não esta conectado');
                            }
                        });
                    }
                }else{
                    user ='';
                    falhaCadastroBio("Sem conexão, tente novamente mais tarde.");
                }
              });
        }else if(data == 0){
            falhaCadastroBio("Sem conexão, tente novamente mais tarde.");
        }else if(data == -1){
            falhaCadastroBio("Nenhum funcionario encontrado para o pin informado.");
        }else{
            falhaCadastroBio("Pin incorreto!");
        }
    });
}

function falhaCadastroBio(erro){
    $('#resultado').html("<button type='button' class='btn btn-danger btn-circle btnResult'><i class='far fa-times-circle fa-5x' aria-hidden='true'></i></button>");
    $('#resultadoText').html(erro);
    document.getElementById('ModalAviso').style.display = 'block';
    setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
    $(".passinput").val("");
    modo = 0;
    $('#pModo').html('Pin de Acesso');
    $.getJSON('/leitura',  function(data) {
    });
}

//======================================== Cadastro Biometrico END ========================================//


//======================================== Modal Control ========================================//
function exibirModalOp(){
    document.getElementById('ModalOpcoes').style.display = 'block';
}

function exibirModalCOnfig(){
    document.getElementById('ModalConfig').style.display = 'block';
}


function closeConfig(){
    document.getElementById('ModalConfig').style.display = 'none';
    document.getElementById('altIot').value = '';
    document.getElementById('altAbt').value = '';
}

function closeOp(){
    document.getElementById('ModalOpcoes').style.display = 'none';
}


//======================================== Modal Control END ========================================//


//======================================== Doc Ready ========================================//

//Verifica se uma tentativa de entrada por biometria foi realizada, exibindo o resultado na tela
function verificarEntradaBiometria(){
    $.getJSON('/_verificar_entrada_biometria',  function(data) {
        if(data['success'] == 1 && data['permissao'] == 1){
            $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Bem Vindo!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
            $.getJSON('/Fechar', function(data) {
                 setTimeout(function() {$('#aviso').html('');}, 3000);
            });
        }else if(data == -1){
            $('#resultado').html("<button type='button' class='btn btn-danger btn-circle btnResult'><i class='far fa-times-circle fa-5x' aria-hidden='true'></i></button>");
            $('#resultadoText').html('Digital Invalida!');
            document.getElementById('ModalAviso').style.display = 'block';
            setTimeout(function() {document.getElementById('ModalAviso').style.display = 'none';}, 4000);
            setTimeout(function() {$('#resultadoText').html('');}, 4000);
        }
    });
}

function keypad(){
    $.getJSON('/_keypad',  function(data) {
            if(data["#"] == 1){
                if(modo == 1){
                    validarPinCadastro(data["valor"]);
                }else if(modo == 2){
                    changeIot(data["valor"]);
                }else if(modo == 3){
                    changeDoorTime(data["valor"]);
                }else{
                    exibirModalOp();
                }
                keypadReset();
            }else{
                $(".passinput").val(data["valor"]);
                if(data["valor"].length == 4 && modo == 0){
                    entrar(data["valor"]);
                    keypadReset();
                }
            }

    });
}

function keypadReset(){
    $.getJSON('/_keypad_reset',  function(data) {
        modo = 0;
        $('#pModo').html('Pin de Acesso');
        $(".passinput").val("");
        document.getElementById('inputPass').style.display = 'block';
        document.getElementById('inputOp').style.display = 'none';
    });
}

//Verifica se o dispositivo esta conectado
function checkConnection(){
    $.getJSON('/_get_conexao',  function(data) {
        document.getElementById('con').innerHTML = data.toString();
    });
}

//Atualiza horario
function relogio(){
    var dia = new Date();
    var h = dia.getHours();
    var m = dia.getMinutes();

    var dd = dia.getDate();

    var mm = dia.getMonth()+1;
    var yyyy = dia.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = dd + '/' + mm + '/' + yyyy;

    h = checkTime(h);
    m = checkTime(m);

    document.getElementById('hora').innerHTML = h + ":" + m;
    document.getElementById('data').innerHTML = today;
}

//Formata data/hora
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

$(document).ready(function() {
    setInterval(keypad, 100);
    setInterval(relogio, 1000);
    setInterval(verificarEntradaBiometria, 1000);
    setInterval(checkConnection, 30000);
});
//======================================== Doc Ready END ========================================//

function exibirTecladoEntrada(){
    document.getElementById('pinEntrada').value = '';
    var modal = document.getElementById('ModalTecladoEntrada');
    modal.style.display = "block";
}

function fecharTecladoEntrada(){
    var modal = document.getElementById('ModalTecladoEntrada');
    modal.style.display = "none";
}

function tecladoInputEntrada(valor){
    var text = document.getElementById('pinEntrada').value;
    text += valor;
    document.getElementById('pinEntrada').value = text;
}

function confirmaInputEntrada(){
    var text = document.getElementById('pinEntrada').value;
    document.getElementById('inputPass').value = text;
    fecharTecladoEntrada();
    entrarTecladoPin(text);
}

function apagarPinEntrada(){
    var text = document.getElementById('pinEntrada').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('pinEntrada').value = text;
}

function apagarPinEntrada(){
    var text = document.getElementById('pinEntrada').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('pinEntrada').value = text;
}

function tecladoAltAbt(valor){
    var text = document.getElementById('altAbt').value;
    text += valor;
    document.getElementById('altAbt').value = text;
}

function apagarAltAbt(){
    var text = document.getElementById('altAbt').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('altAbt').value = text;
}

function tecladoAltIot(valor){
    var text = document.getElementById('altIot').value;
    text += valor;
    document.getElementById('altIot').value = text;
}

function apagarAltIot(){
    var text = document.getElementById('altIot').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('altIot').value = text;
}

function tecladoInputCadastro(valor){
    var text = document.getElementById('pinCadastro').value;
    text += valor;
    document.getElementById('pinCadastro').value = text;
}

function tecladoCadastro(){
    document.getElementById('pinCadastro').value = '';
    var modal = document.getElementById('ModalTecladoCadastro');
    modal.style.display = "block";
}

function apagarTecladoCadastro(){
    var text = document.getElementById('pinCadastro').value;
    text = text.substring(0, text.length - 1);
    document.getElementById('pinCadastro').value = text;
}

function fecharTecladoCadastro(){
    var modal = document.getElementById('ModalTecladoCadastro');
    modal.style.display = "none";
}

function confirmarCadastro(){
    var text = document.getElementById('pinCadastro').value;
    document.getElementById('pinCad').value = text;
    validarPinCadastro(pin);
}
/*function changeIotTeclado(){
    modo = 2;
    $('#pModo').html('Código IOT');
    closeConfig();
    closeOp();
    document.getElementById('inputPass').style.display = 'none';
    document.getElementById('inputOp').style.display = 'block';
}*/

function changeIot(altIot){
    $.getJSON('/changeIOT', {iot:altIot},  function(data) {
        closeConfig();
        closeOp();
        $('#lblDevice').html(altIot);
        document.getElementById('altIot').value = '';
        $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
        $('#resultadoText').html('N° do IOT alterado com sucesso!');
        document.getElementById('ModalAviso').style.display = 'block';
        setTimeout(function() {location.reload();}, 4000);
    });
}

/*function changeTimeTeclado(){
    modo = 3;
    $('#pModo').html('Tempo de Abertura');
    closeConfig();
    closeOp();
    document.getElementById('inputPass').style.display = 'none';
    document.getElementById('inputOp').style.display = 'block';
}*/

function changeDoorTime(altAbt){
    $.getJSON('/changeDoorTime', {time:altAbt},  function(data) {
        closeConfig();
        closeOp();
        $('#lblTimer').html(altAbt + 's');
        document.getElementById('altAbt').value = '';
        $('#resultado').html("<button type='button' class='btn btn-success btn-circle btnResult'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button>");
        $('#resultadoText').html('Tempo de abertura da porta atualizado com sucesso!');
        document.getElementById('ModalAviso').style.display = 'block';
        setTimeout(function() {document.getElementById('ModalAviso').style.display = "none";}, 4000);
    });
}

function confirmarAlteracao(){
    var iotConfirma = document.getElementById('altIot').value;
    var tempoConfirma = document.getElementById('altAbt').value;
    if(iotConfirma!='')
    {
        changeIot(iotConfirma);
    }
    if(tempoConfirma!='' && tempoConfirma>0)
    {
        changeDoorTime(tempoConfirma);
    }
}

/**
 * Get the user IP throught the webkitRTCPeerConnection
 * @param onNewIP {Function} listener function to expose the IP locally
 * @return undefined
 */
function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
        iceServers: []
    }),
    noop = function() {},
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

    function iterateIP(ip) {
        if (!localIPs[ip]) onNewIP(ip);
        localIPs[ip] = true;
    }

     //create a bogus data channel
    pc.createDataChannel("");

    // create offer and set local description
    pc.createOffer(function(sdp) {
        sdp.sdp.split('\n').forEach(function(line) {
            if (line.indexOf('candidate') < 0) return;
            line.match(ipRegex).forEach(iterateIP);
        });

        pc.setLocalDescription(sdp, noop, noop);
    }, noop);

    //listen for candidate events
    pc.onicecandidate = function(ice) {
        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
}

// Usage

getUserIP(function(ip){
	document.getElementById("ip_numero").innerHTML = ip;
});



