var pin = new Array();
var boxdireita;
var user;
var digital;

function calc(valor){
    var j = pin.length;
    if(valor == -1){
        if(j>0){
            pin.pop();
            $(".visor_valor").val(pin.join(""));
        }
    }else if(valor == -2 && j==4){
        entrar(pin.join("").toString());
    }else{
        if(j<4){
            pin.push(valor);
            $(".visor_valor").val(pin.join(""));
        }

    }
}
function entrar(pin){
    $.getJSON('/_validate_pin', {pin:pin}, function(data) {
        if(data == 0){
            $('#aviso1').html('<b>Problemas de Conexão</b>');
            $('#aviso2').html('Tente novamente mais tarde.');
            setTimeout(function() {$('#aviso2').html('');}, 4000);
            setTimeout(function() {$('#aviso1').html('Informe um pin para iniciar o cadastro.');}, 4000);
        }else if(data != null){
            user = data;
            $('#aviso1').html('Iniciando cadastro do usuário: <b>'+ data +'</b>');
            $.getJSON('/_count_digitais', {user:user}, function(data) {
                digital = data;
                $('#aviso2').html('Digitais Cadastradas: '+data+'/2.');

                $('.visor_valor').val('');
                pin = [];
                if(data == 2){
                    $('#aviso3').html('Quantidade maxima de digitais cadastradas.');
                    user ='';
                    setTimeout(function() {$('#aviso3').html('');}, 4000);
                    setTimeout(function() {$('#aviso2').html('');}, 4000);
                    setTimeout(function() {$('#aviso1').html('Informe um pin para iniciar o cadastro.');}, 4000);
                    return;
                }else{
                    $('#aviso3').html('Posicione o dedo sobre o leitor.');
                    cadastrar_digital00();
                }
              });
        }else{
            $('#aviso2').html('Nenhum usuário encontrado para o pin informado');
            setTimeout(function() {$('#aviso2').html('');}, 3000);
            pin = [];
            $.getJSON('/_leitura',function(data) {

            });
        }
    });
}

//Registra primeira imagem para criar template
function cadastrar_digital00(){
    $.getJSON('/_read_finger0',{user:user},function(data) {
        if(data == 1){
            $('#aviso3').html('Remova o dedo do leitor.');
            setTimeout(function() {cadastrar_digital01();}, 2000);
        }else{
            $('#aviso3').html('Erro ao realizar cadastro');
            setTimeout(function() {$('#aviso3').html('');}, 4000);
            setTimeout(function() {$('#aviso2').html('');}, 4000);
            setTimeout(function() {$('#aviso1').html('Informe um pin para iniciar o cadastro.');}, 4000);
            user ='';
            $.getJSON('/_leitura',function(data) {

            });
        }
    });
}

//Registra segunda imagem e cria template
function cadastrar_digital01(){
    var quant = parseInt(digital) + 1
    $('#aviso3').html('Posicione novamente o dedo sobre o leitor.');
    $.getJSON('/_read_finger1',{user:user,digital:digital},function(data) {
        if(data == 1){
            $('#aviso2').html('Digitais Cadastradas: '+quant+'/2.');
            $('#aviso3').html('Cadastro realizado com sucesso!');
            setTimeout(function() {$('#aviso3').html('');}, 4000);
            setTimeout(function() {$('#aviso2').html('');}, 4000);
            setTimeout(function() {$('#aviso1').html('Informe um pin para iniciar o cadastro.');}, 4000);
            pin [];
            user ='';
            $.getJSON('/_leitura',function(data) {

            });
        }else{
            $('#aviso3').html('Erro ao realizar cadastro');
            setTimeout(function() {$('#aviso3').html('');}, 4000);
            setTimeout(function() {$('#aviso2').html('');}, 4000);
            setTimeout(function() {$('#aviso1').html('Informe um pin para iniciar o cadastro.');}, 4000);
            pin = [];
            user ='';
            $.getJSON('/_leitura',function(data) {

            });
        }
    });
}

function verificar(){
    $.getJSON('/_check_fingerprint',function(data) {
        if(data == 1){
            $('#aviso4').html('match!');
        }else{
            $('#aviso4').html('nop');
        }
    });
}

$(document).ready(function() {
    boxdireita =  $(".boxdireita2").html();
    function ajustaDataHora() {

    $.getJSON('/_get_data',  function(data) {
      document.getElementById('data').innerHTML = data.toString();
    });

    $.getJSON('/_get_hora',  function(data) {
      document.getElementById('hora').innerHTML = data.toString();
    });

    $.getJSON('/_get_conexao',  function(data) {
      document.getElementById('con').innerHTML = data.toString();
    });

    setTimeout(ajustaDataHora, 30000);
    }

    setTimeout(ajustaDataHora, 30000);
});