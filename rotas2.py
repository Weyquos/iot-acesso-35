# from __future__ import print_function
# from wifi import Cell, Scheme
# from vectors import vectors

# from wireless import Wireless
import datetime
import json
import sqlite3
import subprocess
import sys
import threading
import urllib
import os
import socket
import re
from threading import Timer
import os
import requests

from subprocess import check_output
import RPi.GPIO as GPIO
import pymysql as pymysql
import pymysql.cursors
import requests
from click._compat import raw_input
from flask import Flask, request, render_template, jsonify, json
from gpiozero import LED
import time
import pykeypi as teclado

from subprocess import check_output

from Model.Usuario import Usuario

# from pyfingerprint import PyFingerprint
from pyfingerprint.pyfingerprint import PyFingerprint

app = Flask(__name__)

leitorStatus = False  # Indica se o dispositivo esta conectado com o leitor biometrico

f = None  # Inicializa variavel para o objeto do leitor

iotID = 1565  # IOT ID(Define a sala que dispositivo esta gerenciando)

doorTime = 5  # Tempo, em segundos, que a porta se mantem aberta após liberação

updateHora = 2  # Hora do update automatico

updateMin = 10  # Min do update automatico

doorStatus = False  # Indica se a porta deve estar fechada(FALSE) ou aberta (TRUE)

update = False  # Quando True inicia update das digitais cadastradas no leitor local

modo = 'Leitura'  # Modo do leitor biometrico(Leitura / Cadastro)

listaPermissoes = []  # Usuario com permissão de acesso na sala

listaPermissoesFuncionario = []

digital = 0  # Leitura que esta sendo cadastrada na biometria(primeira ou segunda imagem que sera convertida para o template)

# ip = check_output(['hostname', '-I'])       # Verifica ip do dispositivo
# ip = str(ip).strip('b').strip("'")
# ip = ip[:-3]

resultadoBio = 0  # Registra tentativa de acesso via biometria

iotType = 0  # Tipo de iot (-1 = Falha ao identificar / 0 = Sala Comum / 1 = Sala Dedicada / 2 = Sala de Reunião

posicao = -1

pinKeypad = {'valor': '', '#': 0}

Admin = Usuario(5, 'Admin', '', '--')

sqlitePath = ----

# Registra erros ocorridos durante a execucao do programas

GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def registrarLogErro(e):
    try:
        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()

        sql = "INSERT INTO log(erro, hora) VALUES (?,?)"
        cursor.execute(sql, (e, datetime.datetime.now()))
        conn.commit()

        conn.close()
    except Exception as e:
        print(e)


try:
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(32, GPIO.OUT)
    GPIO.output(32, GPIO.HIGH)
except Exception as error:
    registrarLogErro(error)

reader = sqlite3.connect(sqlitePath, isolation_level=None)
reader.execute('pragma journal_mode=wal;')
reader.close()

# ==================================Produção=================================#
iotUrl = --
ipbanco = --
userbanco = --
passbanco = --
db = --
charset = 'utf8mb4'
# ===========================================================================#

'''
Verifica se o dispositivo possui configuracoes locais anteriotes. Caso existam, esses valores sao utilizados.
iotID       -> Define qual o dispositivo atual. 
doorTime    -> Defiene o tempo de abertura da porta.
updateHora  -> Hora programada para update automatico.      ==REMOVER==
updateMin   -> Minuto programado para update automatico.    ==REMOVER==
ipbanco     -> Define IP de acesso ao banco de dados.       ==REMOVER==
userbanco   -> Define usuario de acesso ao banco de dados.  ==REMOVER==
passbanco   -> Define senha de acesso ao banco de dados.    ==REMOVER==
db          -> Define banco de dados a ser utilizado.       ==REMOVER==
charset     -> Define charset a ser utilizado no banco.     ==REMOVER==
'''


def verificar_ip():
    mine = os.popen('ifconfig wlan0 | grep "inet 10" | cut -c 14-25')
    myip = mine.read()
    return myip


ip = verificar_ip()


def verificar_configuracoes_locais():
    global iotID
    global doorTime
    global updateHora
    global updateMin
    global ipbanco
    global userbanco
    global passbanco
    global db
    global charset

    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT * FROM configuracao;")
    row = cursorLocal.fetchone()
    if row is None:
        cursorLocal.execute(
            "INSERT INTO configuracao VALUES(:iot,:abertura,:updateH,:updateM,:ip,:user,:pass,:database,:charset);",
            {'iot': iotID, 'abertura': doorTime, 'updateH': updateHora, 'updateM': updateMin, 'ip': ipbanco,
             'user': userbanco, 'pass': passbanco, 'database': db, 'charset': charset})
        conn.commit()
    else:
        iotID = int(row[0])
        doorTime = int(row[1])
        updateHora = int(row[2])
        updateMin = int(row[3])
        ipbanco = row[4]
        userbanco = row[5]
        passbanco = row[6]
        db = row[7]
        charset = row[8]
    conn.close()


verificar_configuracoes_locais()


def writeMsgDebug(msg):
    arquivoTxt = open('/home/pi/msg-debug.txt', 'w')
    htmlMsg = msg
    arquivoTxt.writelines(htmlMsg)
    arquivoTxt.close()

'''
Realiza conexão com leitor biometrico
Erro -> leitorStatus setado para false, impedindo que operações que necessitam dele sejam executadas
'''


def conectarLeitor():
    global leitorStatus
    global f

    try:
        f = PyFingerprint('/dev/ttyS0', 57600, 0xFFFFFFFF, 0x00000000)
        leitorStatus = True
        #writeMsgDebug('Leitor Funcionando')
    except Exception as e:
        registrarLogErro(str(e))
        leitorStatus = False
        #writeMsgDebug('Leitor Não Funcionando')

conectarLeitor()

'''
Thread de controle do update automatico da lista de permissoes
Processo ocorre em intervalos definidos por Interval em seu construtor
'''
class AccessList(object):

    def __init__(self, interval=3600):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        global iotID
        global modo
        with app.app_context():
            try:
                while True:
                    time.sleep(self.interval)
                    if iotType == 1:
                        if iotID != 1565 and modo != 'Cadastro':
                            carregaUser()
                        elif modo != 'Cadastro':
                            carregarUserSalaComum()
            except Exception as e:
                registrarLogErro(str(e))
                self.run()


accessUpdate = AccessList()  # Inicia thread de atualização da lista de permissoes

'''
Thread para controlar a abertura da porta
doorStatus:True -> Aciona o rele pelo tempo definido em doorTime
'''


class DoorControl(object):

    def __init__(self, interval=1):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        try:
            global doorStatus
            while True:
                if doorStatus:
                    GPIO.setup(29, GPIO.OUT)
                    GPIO.output(29, GPIO.HIGH)
                    time.sleep(doorTime)
                    GPIO.setup(29, GPIO.OUT)
                    GPIO.output(29, GPIO.LOW)
                    doorStatus = False
                time.sleep(self.interval)
        except Exception as e:
            registrarLogErro(str(e))
            self.run()


doorControler = DoorControl()  # Inicia Thread de Controle da Porta

# Thread para controlar o teclado matricial

class KeyPad(object):

    def __init__(self, interval=1):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        try:
            global pinKeypad
            tecla_pressionada = teclado.keypad()
            while True:
                valor = str(tecla_pressionada.get_key())
                if valor == '*':
                    pinKeypad['valor'] = pinKeypad['valor'][:-1]
                elif valor == '#':
                    pinKeypad["#"] = 1
                    # print('#')
                else:
                    pinKeypad["valor"] += valor
        except Exception as e:
            print(e)
            self.run()


keyPad = KeyPad()  # Inicia Thread de Controle da Porta


'''
Thread que gerencia o leitor biometrico
Modo:Leitura    -> Leitor aguardando leitura de digital para verificar permissão
Modo:Cadastro   -> Leitor aguardando leitura de duoas digitais para gerar template
                Digital:0   -> Aguardando primeira Digital
                Digital:1   -> Aguardando segunda Digital
Update:True     -> Inicia processo de update das digitais armazenadas no leitor



class leitor(object):

    def __init__(self, interval=1):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        global modo
        global digital
        global leitorStatus
        global iotType
        global update
        global updateStatus
        global f
        try:
            conectarLeitor()  # Inicia o leitor biometrico

            while True:
                if leitorStatus:
                    while not f.readImage() and modo == 'Leitura':
                        #writeMsgDebug('Modo não Leitura e readImage')
                        if update:    
                            writeMsgDebug('Update')
                            if iotType == 1:
                                if iotID == 1565:
                                    writeMsgDebug('iot type = 1 e iotID = 1565')
                                    carregarUserSalaComum()
                                else:
                                    writeMsgDebug('iot type = 1')
                                    carregaUser()
                            elif iotType == 2:
                                writeMsgDebug('iot type = 2')
                                update = False
                                updateDigitais()
                                updateStatus = False

                        pass
                    if modo == 'Leitura':
                        #writeMsgDebug('Modo Leitura')
                        f.convertImage(0x01)
                        #teste = f.convertImage(0x01)
                        result = f.searchTemplate()
                        positionNumber = result[0]
                        identificarLeitura(positionNumber)
                        writeMsgDebug(result)

                    while not f.readImage() and modo == 'Cadastro':
                        pass
                    if digital == 0 and modo == 'Cadastro':
                        f.convertImage(0x01)
                        digital = 1
                    if digital == 1 and modo == 'Cadastro':
                        f.convertImage(0x02)
                        digital = 0
                        modo = 'Leitura'
                else:
                    conectarLeitor()

                time.sleep(self.interval)

        except Exception as e:
            registrarLogErro(str(e))
            leitorStatus = False
            self.run()


leitorDigital = leitor()  # Inicializa Thread de controle do leitor
'''

'''
Verifica o nome da empresa de acordo com o iot atual
Atualiza o registro local
Retorno -> Nome da empresa
Erro -> Uso o ultimo nome armazenado no dispositivo
'''


def get_IOT_nome():
    global iotID
    try:
        iot = requests.get(iotUrl + "?classe=IOT&metodo=CheckIOT&atributo[iotid]=" + str(iotID))
        empresa = json.loads(iot.text)['list'][0]['descricaoservico']

        conn = sqlite3.connect(sqlitePath)

        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM empresa;")
        conn.commit()
        cursorLocal.execute("INSERT OR REPLACE INTO empresa VALUES(0,?);", (empresa,))
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        empresa = ''
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT nome FROM empresa;")
        for row in cursorLocal.fetchall():
            empresa = row[0]
        conn.close()
    return empresa


# Função de teste para controle de permissões
def controleAcesso():
    global leitorStatus
    global listaPermissoes
    global iotID

    empresa = get_IOT_nome()

    permissoes = requests.get(
        iotUrl + "?classe=IOT&metodo=ListarUsuariosAutorizadosPorIdAgendaServico&atributo[iotid]=" + str(iotID))
    listaPermissoes.clear()

    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    for user in json.loads(permissoes.text)['list']:
        cod = user['idusuario']
        cursorLocal.execute(
            "SELECT user.id, user.nome, user.pin, '--' FROM usuario user WHERE user.id = " + str(cod) + ";")
        for row in cursorLocal.fetchall():
            usuario = Usuario(row[1], row[0], row[2], row[3])
            listaPermissoes.append(usuario)
    get_admin()
    conn.close()

    atualizar_permissoes_local()
    if leitorStatus:
        updateDigitais()
    return empresa


'''
Carrega permissões de acesso para usuarios em uma sala dedicada
Inicializa update do leitor biometrico caso exista conexão e o leitor esteja conectado
Retorno -> Nome da empresa da sala
Erro -> Carrega as permissões de acesso da ultima execução bem sucedida
'''


def carregaUser():
    global leitorStatus
    global listaPermissoes
    global updateStatus
    global update
    empresa = get_IOT_nome()

    update = False
    try:
        permissoes = requests.get(
            iotUrl + "?classe=IOT&metodo=ListarUsuariosAutorizadosPorIdAgendaServico&atributo[iotid]=" + str(iotID))
        listaPermissoes.clear()
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        for user in json.loads(permissoes.text)['list']:
            cod = user['idusuario']
            cursorLocal.execute("SELECT user.id, user.nome, user.pin, '--' FROM usuario user WHERE user.id = ?;",
                                (cod,))
            for row in cursorLocal.fetchall():
                usuario = Usuario(row[1], row[0], row[2], row[3])
                listaPermissoes.append(usuario)
        conn.close()
        get_admin()

        atualizar_permissoes_local()
        updateFuncionariosPermissoes()
        if leitorStatus:
            updateDigitais()
    except Exception as e:
        registrarLogErro(str(e))
        carregar_permissoes_local()
    updateStatus = False
    return empresa


'''
Carrega para a lista de permissoes todos os usuarios vinculados com um contrato Vigente
Inicializa update do leitor biometrico caso exista conexão e o leitor esteja conectado
Retorno -> Nome da empresa da sala
Erro -> Carrega as permissões de acesso da ultima execução bem sucedida
'''


def carregarUserSalaComum():
    global update
    global leitorStatus
    global updateStatus
    global listaPermissoes
    update = False
    empresa = get_IOT_nome()
    try:
        listaPermissoes.clear()
        # conn = sqlite3.connect(sqlitePath)
        # cursorLocal = conn.cursor()
        # cursorLocal.execute("SELECT user.id, user.nome, user.pin, '--' FROM usuario user;")
        # for row in cursorLocal.fetchall():
        #     usuario = Usuario(row[1], row[0], row[2], row[3])
        #     listaPermissoes.append(usuario)
        # conn.close()
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/get-usuarios-iot.php")
        for user in json.loads(usuarios.text):
            usuario = Usuario(user["Nome"], int(user["Cod"]), user["Pin"], '--')
            listaPermissoes.append(usuario)
        get_admin()
        get_Comercial()
        atualizar_permissoes_local()
        updateFuncionariosPermissoes()
        if leitorStatus:
            updateDigitais()
    except Exception as e:
        carregar_permissoes_local()
        registrarLogErro(str(e))
    updateStatus = False
    return empresa


'''
Renderiza a tela inicial da aplicação
iotType:0 -> Sala Comum.
iotType:1 -> Sala Dedicada.
iotType:2 -> Sala de Reunião.
'''


@app.route('/_openKeyb')
def openKeyb():
    subprocess.call('onboard', shell=True)
    return jsonify(0)


@app.route('/')
def index():
    global ip
    checkIotType()
    if iotType == 0:
        return jsonify(0)
    elif iotType == 1:
        if iotID == 1565:
            empresa = carregarUserSalaComum()
        else:
            empresa = carregaUser()
        return render_template('estrutura35.html', data=time.strftime("%d/%m/%Y"), hora=time.strftime("%H:%M"),
                               conexao="Online", versao="1.0.0", device=str(iotID), ip=str(ip), timer=doorTime)
    else:
        return jsonify(0)


'''
Verifica se o pin informado tem permissão para acessar a sala. 
Result:0 -> Tentativa de entrada, PIN invalido.
Result:1 -> Entrada em sala dedicada.
Result:2 -> Entrada em sala de Reunião.
Result:3 -> Usuario admin, libera a porta.
'''


@app.route('/_validate_pin')
def validate_pin():
    global listaPermissoes
    global iotID
    try:
        pin = request.args.get('pin')
        if pin == Admin.pin:
            resposta = {'nome': 'Admin', 'result': 3, 'cod': 0}
            return jsonify(resposta)
        for user in listaPermissoes:
            if user.pin == pin:
                tipo = getTipoUsuario(user.cod)
                valor, statusC, desconto, statusD = VerificarCOnfiguracoesSigv()
                if tipo == 'Y' and int(statusC) == 1565:
                    if iotID == 1:
                        '''
                        cred = VerificarCreditos(int(user.cod), valor)
                        if cred == 0:
                            resposta = {'nome': user.nome, 'result': 4, 'permissao': 0,
                                        'msg': 'Você não possui créditos para acessar essa unidade.'}
                        elif cred == 1:
                            resposta = {'nome': user.nome, 'result': 4, 'permissao': 1,
                                        'msg': 'R$' + str(valor) + ' foram utilizados.'}
                        else:
                        '''
                        resposta = {'nome': user.nome, 'result': 4, 'permissao': 1, 'msg': 'Bem Vindo !'}
                    else:
                        resposta = {'result': 1, 'nome': user.nome, 'permissao': 0}
                    return jsonify(resposta)
                else:
                    if iotType == 2:
                        resposta = {'nome': user.nome, 'result': 2, 'cod': user.cod}
                        return jsonify(resposta)
                    else:
                        resposta = {'nome': user.nome, 'result': 1}
                        return jsonify(resposta)
    except Exception as e:
        registrarLogErro(e)
    resposta = {'nome': '--', 'result': 0}
    return jsonify(resposta)


'''
Verifica se o usuario possui créditos suficientes para acesso.
0 -> Creditos insuficientes
1 -> Tem creditos para acessar
2 -> Ja possui créditos para essa data


def VerificarCreditos(cod, valor):
    try:
        creditoDia = requests.get(
            "http://alugueap.com/userpweb/api/rasp/verificar-credito-temporario-dia.php?idUsuario=" + str(cod)).text
        if creditoDia != "-1":
            return 2

        dados = requests.get("http://alugueap.com/userpweb/api/rasp/get-dados-usuario-club.php?idUsuario=" + str(cod))
        cred = float(json.loads(dados.text)['Credito'])
        clubID = int(json.loads(dados.text)['Ydc'])
        pin = json.loads(dados.text)['pin']
        if float(cred) < float(valor):
            return 0
        return int(requests.get("http://alugueap.com/userpweb/api/rasp/adicionar-credito-temporario.php?ClubId=" + str(
            clubID) + "&pin=" + str(pin) + "&valor=" + str(valor)).text)
    except Exception as e:
        registrarLogErro(e)
        return 0
'''

def VerificarCOnfiguracoesSigv():
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT * FROM sigvConfig;")
    row = cursorLocal.fetchone()
    valor = float(row[0])
    statusC = int(row[1])
    desconto = int(row[2])
    statusD = int(row[3])
    conn.close()
    return valor, statusC, desconto, statusD


def getTipoUsuario(cod):
    tipo = ''
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    sql = "SELECT tipo FROM usuario WHERE id = ?;"
    cursor.execute(sql, (cod,))
    for row in cursor.fetchall():
        tipo = row[0]
    conn.close()
    return tipo

'''
Valida pin para exibição da tela de configurações
Return:1 -> Pin de administrador correto.
Return:0 -> Pin incorreto.
'''


@app.route('/_validate_pin_admin')
def validade_pin_admin():
    pin = request.args.get("pin")
    if pin == Admin.pin:
        return jsonify(1)
    else:
        return jsonify(0)


'''
Libera a porta pelo tempo definido em doorTime
Ao setar doorStatus = True a Thread de controle da porte realiza o processo
'''


@app.route('/Fechar')
def abrir_fechar():
    global doorStatus
    doorStatus = True
    return jsonify(1)


'''
Libera Thread de controle do leitor para iniciar update
Return:0 -> Leitor não conectado
Return:1 -> Leitor conectado, libera update.
'''


@app.route('/_update')
def updateAcessos():
    global update
    global updateStatus

    update = True
    updateStatus = True
    time.sleep(3)
    #while updateStatus:

    if iotID == 1565:
        empresa = carregarUserSalaComum()
    else:
        empresa = carregaUser()

    return jsonify(1)


'''
@app.route('/_update')
def updateAcessos():
    global update
    global updateStatus

    update = True
    updateStatus = True
    #writeMsgDebug('Atualizou base digital')
    time.sleep(3)
    while updateStatus:
        if not leitorStatus:
            updateStatus = False
            #writeMsgDebug('Não atualizou base digital')
            return jsonify(0)
        pass
    return jsonify(1)
'''
'''
Verifica se é possivel se copnectar com a API
Caso a conexão seja bem sucedida os dados de inicio e fim de reunião em periodo offline são atualizados
'''


@app.route('/_get_conexao')
def get_conexao():
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)
        con = "Online"
    except urllib.error.URLError as e:
        print(e)
        con = "Offline"
    return jsonify(con)


# Verifica a data atual
@app.route('/_get_data')
def get_data():
    return jsonify(time.strftime("%d/%m/%Y"))


'''
Coloca a Thread de controle do leitor em modo de cadastro
'''


@app.route('/cadastro')
def rend_cadastro():
    global modo
    global updateStatus
    if updateStatus:
        return jsonify(0)
    modo = 'Cadastro'
    return jsonify(1)


'''
Coloca a Thread de controle do leitor em modo de leitura
'''


@app.route('/leitura')
def leitura():
    global modo
    #modo = 'Leitura'
    writeMsgDebug('Modo Leitura')
    return jsonify(1)


# Verifica o horario atual
@app.route('/_get_hora')
def get_hora():
    return jsonify(time.strftime("%H:%M"))


# Reinicia o dispositivo
@app.route('/reboot')
def reboot():
    os.system('sudo reboot')
    return jsonify(1)


'''
Altera valor de doorTime
doorTime -> Define o tempo que o rele fica acionado para abrir a porta
'''


@app.route('/changeDoorTime')
def changeDoorTime():
    global doorTime
    global iotID
    openTime = request.args.get('time')
    doorTime = int(openTime)

    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "UPDATE configuracao SET tempoAbertura = ? WHERE iotID = ?;"
    cursor.execute(sql, (doorTime, iotID))
    conn.commit()

    conn.close()

    return jsonify(1)


'''
Altera valor de iotID
doorTime -> Define o dispositivo que o iot esta gerenciando.
'''


@app.route('/changeIOT')
def chengeIOT():
    global iotID
    idNovoIOT = request.args.get('iot')
    iotID = idNovoIOT

    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "UPDATE configuracao SET iotID = ?;"
    cursor.execute(sql, (iotID,))
    conn.commit()

    conn.close()

    return jsonify(1)


@app.route('/att_app')
def attApp():
    path = "/home/pi"
    clone = "git clone https://gitlab-ci-token:x7QJwEywsz34JmKsGD-P@gitlab.com/desenvolvimentoyoudo/controle-acesso" \
            "-35.git "

    os.system("rm -rf /home/pi/controle-acesso-35")
    os.system("rm -rf /home/pi/iotyoudo.db")
    os.chdir(path)
    os.system(clone)
    os.system('sudo reboot')

    return jsonify(1)


'''
Atualiza as digitais do leitor de acordo com a lista de permissoes.
Relaciona a posição em que a digital foi inserida no leitor com o usuario a qual pertence
'''


def updateDigitais():
    global posicao
    global listaPermissoes
    cnx = None
    cursor = None
    cont = 0
    try:
        f.clearDatabase()
        resetPosicoesBio()
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)
        cursor = cnx.cursor()
        cursor.execute(
            "select du.idusuario as Cod,(select digital from tab_ydo_digital_usuario where numerodigital = 0 and idusuario = du.idusuario) as d0, (select digital from tab_ydo_digital_usuario where numerodigital = 1 and idusuario = du.idusuario) as d1 from tab_ydo_digital_usuario du group by du.idusuario;")
        result_set = cursor.fetchall()
        for row in result_set:
            idUsuario = row["Cod"]

            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute("INSERT OR IGNORE INTO usuarioPosicao VALUES(?,?,?)", (idUsuario, None, None))
            conn.commit()

            if row['d0'] is not None:
                f.uploadCharacteristics(0x02, eval(row['d0']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd0 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            if row['d1'] is not None:
                f.uploadCharacteristics(0x02, eval(row['d1']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd1 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    updateDigitaisFuncionarios(cont)


'''
Atualiza a lista de funcinarios que possuem permissão a unidade.
'''


def updateFuncionariosPermissoes():
    global iotID
    global listaPermissoesFuncionario
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "SELECT f.codfuncionario as Codigo FROM tb_funcionarios_acessos ac INNER JOIN tb_funcionarios f on f.codfuncionario = ac.codfuncionario WHERE ac.locca_id = %s",
            [iotID])

        result_set = cursor.fetchall()
        listaPermissoesFuncionario = []
        for row in result_set:
            codFuncionario = row["Codigo"]
            listaPermissoesFuncionario.append(codFuncionario)
        atualizarPermissoesFuncionarioLocal()
    except Exception as e:
        registrarLogErro(str(e))
        carregarPermissoesFuncionarioLocal()
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


'''
Atualiza a base local com a lista de funcionarios com acesso à unidade.
'''


def atualizarPermissoesFuncionarioLocal():
    global listaPermissoesFuncionario
    ids = []
    for func in listaPermissoesFuncionario:
        acesso = {'id': 0, 'cod': func}
        ids.append(acesso)

    conn = sqlite3.connect(sqlitePath)

    cursorLocal = conn.cursor()
    cursorLocal.execute("DELETE FROM acessoFuncionario;")
    conn.commit()
    cursorLocal.executemany("INSERT OR REPLACE INTO acessoFuncionario VALUES(:id,:cod);", ids)
    conn.commit()

    conn.close()


'''
Carraga as permissoes de funcionarios com base no armazenamento local(ultima atualização efetivada)
'''


def carregarPermissoesFuncionarioLocal():
    global listaPermissoesFuncionario
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT funcionario FROM acessoFuncionario;")
    for row in cursorLocal.fetchall():
        listaPermissoesFuncionario.append(row[0])
    conn.close()


'''
Atualiza as digitais do leitor de acordo com a lista de permissoes.
Relaciona a posição em que a digital foi inserida no leitor com o funcionario a qual pertence
'''


def updateDigitaisFuncionarios(cont):
    global posicao
    cnx = None
    cursor = None
    updateFuncionariosPermissoes()
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select func.codfuncionario as Cod, func.nome as Nome, (select digital from tab_ydo_digital_funcionario where numerodigital = 0 and idfuncionario = func.codfuncionario) as d0, (select digital from tab_ydo_digital_funcionario where numerodigital = 1 and idfuncionario = func.codfuncionario) as d1  from tb_funcionarios func inner join tab_ydo_digital_funcionario on tab_ydo_digital_funcionario.idfuncionario = func.codfuncionario group by func.codfuncionario;")

        result_set = cursor.fetchall()
        for row in result_set:
            codFuncionario = row["Cod"]

            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute("INSERT OR IGNORE INTO funcionarioPosicao VALUES(?,?,?)", (codFuncionario, None, None))
            conn.commit()

            if row['d0'] is not None:
                f.uploadCharacteristics(0x02, eval(row['d0']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update funcionarioPosicao SET pd0 = ? WHERE idFuncionario = ?;",
                                    (posicao, codFuncionario))
                conn.commit()
                cont = cont + 1
            if row['d1'] is not None:
                f.uploadCharacteristics(0x02, eval(row['d1']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update funcionarioPosicao SET pd1 = ? WHERE idFuncionario = ?;",
                                    (posicao, codFuncionario))
                conn.commit()
                cont = cont + 1
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


'''
Valida se o pin informado é valido para iniciar o cadastro de digital do usuario
Return:Usuario  ->Nome: nome do usuario
                ->Cod:  id do usuario
Return:-1       ->Pin não é valido
'''


@app.route('/_validate_pin_cadastro')
def get_user():
    global iotType
    nome = ''
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)

        pin = request.args.get('pin')
        iot = requests.get(iotUrl + "?classe=IOT&metodo=ConsultarPinUsuarioByPin&atributo[pin]=" + pin)
        cod = json.loads(iot.text)['list']['idusuario']
        if cod is None or cod == 'null':
            return jsonify(-1)

        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()
        cursor.execute("SELECT user.nome FROM usuario user WHERE user.id = ?;", (cod,))
        for row in cursor.fetchall():
            nome = row[0]
        usuario = {'nome': nome, 'cod': cod, 'type': iotType}
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    return jsonify(usuario)


'''
Valida se o código informado é valido para iniciar o cadastro de digital do funcionario
Return:Usuario  ->  Nome: nome do funcionario
                ->  Cod:  codigo do funcionario
Return:0        ->  Erro
'''


@app.route('/_validate_pin_cadastro_funcionario')
def get_funcionario():
    global iotType
    nome = ''
    codigo = request.args.get('codigo')
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)

        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()
        cursor.execute("SELECT func.nome FROM funcionario func WHERE id = ?;", (codigo,))
        for row in cursor.fetchall():
            nome = row[0]
        usuario = {'nome': nome, 'cod': codigo, 'type': iotType}
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    return jsonify(usuario)


'''
Aguarda a primeira leitura biometrica para realizacao do cadastro
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processi realizado com sucesso
'''


@app.route('/_read_finger0')
def get_finger0():
    global digital
    global leitorStatus
    global modo
    global bioCancelada
    digital = 0

    if leitorStatus:
        while digital == 0:
            if not leitorStatus:
                return jsonify(0)
            if bioCancelada:
                bioCancelada = False
                modo = 'Leitura'
                return jsonify(2)
            pass
    else:
        return jsonify(0)
    return jsonify(1)


'''
Cria o template da digital para o cadstro, armazenando localmente e no banco
Return:-1 -> Digital já esta cadastrada
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processi realizado com sucesso
Return:2 -> As amostras para a criação do template não são referentes ao mesmo dedo
'''


@app.route('/_read_finger1')
def get_finger1():
    global digital
    global leitorStatus
    cnx = None
    try:
        user = request.args.get('user')
        digi = request.args.get('digital')
        while digital == 1:
            if not leitorStatus:
                return jsonify(0)
            pass

        if f.compareCharacteristics() == 0:
            return jsonify(2)

        f.createTemplate()
        characteristics = str(f.downloadCharacteristics(0x01)).encode('utf-8')
        idLast = get_last_digital_id()

        result = f.searchTemplate()
        positionNumber = result[0]

        if positionNumber >= 0:
            return jsonify(-1)

        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)
        with cnx.cursor() as cursor:
            query = "INSERT INTO tab_ydo_digital_usuario(iddigitalusuario,idusuario,numerodigital,digital) VALUES(%s,%s,%s,%s)"
            args = (idLast, user, digi, characteristics)
            cursor.execute(query, args)
        cnx.commit()

        position = f.storeTemplate()

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("INSERT OR IGNORE INTO usuarioPosicao VALUES(?,?,?)", (user, None, None))
        conn.commit()

        if int(digi) == 0:
            cursorLocal.execute("Update usuarioPosicao SET pd0 = ? WHERE idUsuario = ?;", (position, user))
            conn.commit()
        if int(digi) == 1:
            cursorLocal.execute("Update usuarioPosicao SET pd1 = ? WHERE idUsuario = ?;", (position, user))
            conn.commit()
        conn.close()

    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    finally:
        if cnx is not None:
            cnx.close()
    return jsonify(1)


'''
Cria o template da digital para o cadstro, armazenando localmente e no banco
Return:-1 -> Digital já esta cadastrada
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processi realizado com sucesso
Return:2 -> As amostrar para a criação do template não são referentes ao mesmo dedo
'''


@app.route('/_read_finger1_funcionario')
def get_finger1_funcionario():
    global digital
    global leitorStatus
    cnx = None
    try:
        user = request.args.get('user')
        digi = request.args.get('digital')
        while digital == 1:
            if not leitorStatus:
                return jsonify(0)
            pass
        if f.compareCharacteristics() == 0:
            return jsonify(2)
        f.createTemplate()

        characteristics = str(f.downloadCharacteristics(0x01)).encode('utf-8')
        idLast = get_last_digital_funcionario_id()

        result = f.searchTemplate()
        positionNumber = result[0]

        if positionNumber >= 0:
            return jsonify(-1)

        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)
        with cnx.cursor() as cursor:
            query = "INSERT INTO tab_ydo_digital_funcionario(iddigitalfuncionario,idfuncionario,numerodigital,digital) VALUES(%s,%s,%s,%s)"
            args = (idLast, user, digi, characteristics)
            cursor.execute(query, args)
        cnx.commit()

        position = f.storeTemplate()

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("INSERT OR IGNORE INTO funcionarioPosicao VALUES(?,?,?)", (user, None, None))
        conn.commit()
        if int(digi) == 0:
            cursorLocal.execute("Update funcionarioPosicao SET pd0 = ? WHERE idFuncionario = ?;", (position, user))
            conn.commit()
        if int(digi) == 1:
            cursorLocal.execute("Update funcionarioPosicao SET pd1 = ? WHERE idFuncionario = ?;", (position, user))
            conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    finally:
        if cnx is not None:
            cnx.close()
    return jsonify(1)


'''
Retorna o ultimo id registrado na tabela de digitais de usuarios
'''


def get_last_digital_id():
    idLast = 0
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("SELECT coalesce(MAX(iddigitalusuario), 0) as id FROM tab_ydo_digital_usuario")

        result_set = cursor.fetchall()
        for row in result_set:
            idLast = row["id"]
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return idLast + 1


'''
Retorna o ultimo id registrado na tabela de digitais de funcionarios
'''


def get_last_digital_funcionario_id():
    idLast = 0
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("SELECT coalesce(MAX(iddigitalfuncionario), 0) as id FROM tab_ydo_digital_funcionario")

        result_set = cursor.fetchall()
        for row in result_set:
            idLast = row["id"]
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return idLast + 1


'''
Retorna a quantidade de digitais cadastradas para o usuario. A quantidade maxima permitida é 2 cadastros.
'''


@app.route('/_count_digitais')
def get_count_digitais_usuario():
    global f
    global leitorStatus
    cursor = None
    cnx = None
    user = request.args.get('user')
    cont = 0
    try:
        f.generateRandomNumber()
        if not leitorStatus:
            return jsonify(-1)
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("select COUNT(idusuario) as digitais from tab_ydo_digital_usuario where idusuario = %s", [user])

        result_set = cursor.fetchall()
        for row in result_set:
            cont = row["digitais"]
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(-1)
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()

    return jsonify(cont)


'''
Retorna a quantidade de digitais cadastradas para o funcionario. A quantidade maxima permitida é 2 cadastros.
'''


@app.route('/_count_digitais_funcionario')
def get_count_digitais_funcionario():
    global leitorStatus
    if not leitorStatus:
        return jsonify(-1)
    cursor = None
    cnx = None
    user = request.args.get('user')
    cont = 0
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select COUNT(idfuncionario) as digitais from tab_ydo_digital_funcionario where idfuncionario = %s", [user])

        result_set = cursor.fetchall()
        for row in result_set:
            cont = row["digitais"]

    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()

    return jsonify(cont)


'''
Identifica a posição da digital reconhecida.
resultadoBio: -1 -> Digital não foi reconhecida.
resultadoBio:  1 -> Digital reconhecida, sala dedicada.
resultadoBio:  2 -> Digital reconhecida, sala de reunião.
'''


def identificarLeitura(resultPosition):
    global posicao
    global resultadoBio
    global iotType

    if resultPosition == -1:
        resultadoBio = -1
    elif iotType == 1:
        resultadoBio = 1
        posicao = resultPosition
    elif iotType == 2:
        resultadoBio = 2
        posicao = resultPosition


'''
Verifica se ocorreu uma tentativa de acesso biometrico
Return:0 -> Não ocorreu tentativa / leitor em modo de cadastro
Return:-1 -> Nenhuma digital encontrada para a tentativa

Success:1 -> Acesso a sala dedicada
Success:2 -> Acesso a sala de reuniao

func:0 -> Tentativa realizada por um usuario
func:1 -> Tentativa realizada por um funcionario

permissao:0 -> Acesso a unidade negado
permissao:1 -> Acesso a unidade permitido
'''


@app.route('/_verificar_entrada_biometria')
def verificarEntradaBiometrica():
    global resultadoBio
    global posicao
    global modo
    acesso = 0

    if resultadoBio == 0 or modo == 'Cadastro':
        resultadoBio = 0
        return jsonify(0)
    elif resultadoBio == -1:
        resultadoBio = 0
        return jsonify(-1)

    if resultadoBio == 1:
        nome = identificarEntradaBio(posicao)
        resultadoBio = 0
        cod = -1

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute(
            "SELECT usu.id, nome FROM usuario usu INNER JOIN usuarioPosicao posi on posi.idUsuario = usu.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
            (posicao, posicao))
        for row in cursorLocal.fetchall():
            cod = row[0]
        conn.close()

        if cod != -1 and cod is not None:
            for user in listaPermissoes:
                if user.cod == cod:
                    acesso = 1
            resultado = {'success': 1, 'nome': nome, 'permissao': acesso}
            return jsonify(resultado)
        else:
            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute(
                "SELECT func.nome, func.id FROM funcionario func INNER JOIN funcionarioPosicao posi on posi.idFuncionario = func.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
                (posicao, posicao))
            for row in cursorLocal.fetchall():
                cod = row[1]
            conn.close()
            if cod in listaPermissoesFuncionario:
                acesso = 1

        resultado = {'success': 1, 'nome': nome, 'permissao': acesso}
        return jsonify(resultado)
    elif resultadoBio == 2:
        nome = ''
        resultadoBio = 0
        cod = -1

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute(
            "SELECT usu.id, nome FROM usuario usu INNER JOIN usuarioPosicao posi on posi.idUsuario = usu.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
            (posicao, posicao))
        for row in cursorLocal.fetchall():
            cod = row[0]
            nome = row[1]
        conn.close()

        if cod != -1 and cod is not None:
            for user in listaPermissoes:
                if user.cod == cod or int(user.cod) == int(cod):
                    acesso = 1
            resultado = {'success': 2, 'cod': cod, 'nome': nome, 'func': 0, 'permissao': acesso}
            return jsonify(resultado)
        else:
            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute(
                "SELECT func.nome, func.id FROM funcionario func INNER JOIN funcionarioPosicao posi on posi.idFuncionario = func.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
                (posicao, posicao))
            for row in cursorLocal.fetchall():
                nome = row[0]
                cod = row[1]
            conn.close()
            if cod in listaPermissoesFuncionario:
                acesso = 1
            resultado = {'success': 2, 'cod': cod, 'nome': nome, 'func': 1, 'permissao': acesso}
            return jsonify(resultado)


# Verifica qual o tipo do IOT
@app.route('/_check_iot_type')
def checkIotType():
    global iotType
    # TODO Verificar qual atributo identifica o tipo de sala
    if int(iotID) < 1000:
        iotType = 2
    elif int(iotID) > 999:
        iotType = 1
    return jsonify(iotType)


'''
Reinicia os registros que vinculam a posição da digital com o usuario/funcionario
'''


def resetPosicoesBio():
    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "UPDATE usuarioPosicao SET pd0 = null, pd1 = null;"
    cursor.execute(sql)
    sql = "UPDATE funcionarioPosicao SET pd0 = null, pd1 = null;"
    cursor.execute(sql)
    conn.commit()

    conn.close()


'''
Atualiza os registros de permissoes local
'''


def atualizar_permissoes_local():
    ids = []
    for user in listaPermissoes:
        acesso = {'id': 0, 'cod': user.cod}
        ids.append(acesso)

    conn = sqlite3.connect(sqlitePath)

    cursorLocal = conn.cursor()
    cursorLocal.execute("DELETE FROM acesso;")
    conn.commit()
    cursorLocal.executemany("INSERT OR REPLACE INTO acesso VALUES(:id,:cod);", ids)
    conn.commit()

    conn.close()


'''
Atualiza a lista de permissoes com base nos registros locais
'''


def carregar_permissoes_local():
    conn = sqlite3.connect(sqlitePath)

    cursorLocal = conn.cursor()
    cursorLocal.execute(
        "SELECT user.id, user.nome, user.pin, '--' FROM usuario user INNER JOIN acesso a on a.cliente = user.id;")

    for row in cursorLocal.fetchall():
        usuario = Usuario(row[1], row[0], row[2], row[3])
        listaPermissoes.append(usuario)
    get_admin()
    get_Comercial()
    conn.close()


'''
Identifica o usuario que tentou realizar acesso biometrico
'''


def identificarEntradaBio(position):
    nome = ''
    conn = sqlite3.connect(sqlitePath)

    cursorLocal = conn.cursor()

    cursorLocal.execute(
        "SELECT nome FROM usuario usu INNER JOIN usuarioPosicao posi on posi.idUsuario = usu.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
        (position, position))
    for row in cursorLocal.fetchall():
        nome = row[0]

    cursorLocal.execute(
        "SELECT nome FROM funcionario func INNER JOIN funcionarioPosicao posi on posi.idFuncionario = func.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
        (position, position))
    for row in cursorLocal.fetchall():
        nome = row[0]

    conn.close()
    return nome


'''
Verifica os dados do usuario administrador
'''


def get_admin():
    global Admin
    cursor = None
    cnx = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select user.idusuario as Cod,'Admin' as Nome, p.pin as Pin, user.usuario as Email from tab_ydo_usuario user inner join tab_ydo_pin_usuario p on p.idusuario = user.idusuario WHERE user.tipousuario= 'A';")

        result_set = cursor.fetchall()
        for row in result_set:
            nome = row['Nome']
            cod = row['Cod']
            pin = row['Pin']
            email = row['Email']
            Admin = Usuario(str(nome), str(cod), str(pin), str(email))

        atualizarAdminLocal()
    except Exception as e:
        registrarLogErro(str(e))
        carregarAdminLocal()
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


def atualizarAdminLocal():
    global Admin
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM adm;")
        conn.commit()
        cursorLocal.execute("INSERT OR REPLACE INTO adm VALUES(?,?,?);", (Admin.cod, Admin.nome, Admin.pin))
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


def carregarAdminLocal():
    global Admin
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT id, nome, pin FROM adm;")
        for row in cursorLocal.fetchall():
            Admin.cod = row[0]
            Admin.nome = row[1]
            Admin.pin = row[2]
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Verifica os dados do usuario Comercial
'''


def get_Comercial():
    cursor = None
    cnx = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select user.idusuario as Cod,'Comercial YouDO' as Nome, p.pin as Pin, user.usuario as Email from tab_ydo_usuario user inner join tab_ydo_pin_usuario p on p.idusuario = user.idusuario WHERE user.idusuario = 3;")

        result_set = cursor.fetchall()
        for row in result_set:
            nome = row['Nome']
            cod = row['Cod']
            pin = row['Pin']
            email = row['Email']
            Comercial = Usuario(str(nome), str(cod), str(pin), str(email))
            listaPermissoes.append(Comercial)

    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


# Retorna os valores do Keypad
@app.route('/_keypad')
def getPin():
    global pinKeypad
    return jsonify(pinKeypad)


# Reseta o valor do keypad
@app.route('/_keypad_reset')
def resetPin():
    global pinKeypad
    pinKeypad["valor"] = ''
    pinKeypad["#"] = 0
    return jsonify(pinKeypad)


if __name__ == '__main__':
    app.run()
